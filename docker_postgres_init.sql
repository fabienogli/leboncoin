CREATE TABLE type_categories (
    id serial,
    nom text,
    PRIMARY KEY (id)
);

CREATE TABLE marques (
    id serial,
    nom text,
    PRIMARY KEY (id)
);

CREATE TABLE automobiles (
    id serial,
    marque_id int not NULL,
    modele text,
    PRIMARY KEY (id),
    FOREIGN KEY (marque_id) REFERENCES marques(id) ON DELETE CASCADE
);


CREATE TABLE annonces (
    id serial,
    type_categorie_id int NOT NULL,
    categorie_id int,
    titre   text,
    contenu text,
    PRIMARY KEY (id),
    CONSTRAINT fk_type_categorie_id FOREIGN KEY (type_categorie_id) REFERENCES type_categories (id)
);


INSERT INTO type_categories(id, nom) VALUES
(1, 'Emploi'),
(2, 'Immobilier'),
(3, 'Automobile');

INSERT INTO marques(id, nom) VALUES
    (1, 'Audi'),
    (2, 'BMW'),
    (3, 'Citroen');

INSERT INTO automobiles(marque_id, modele) VALUES
    (1, 'Cabriolet'),
    (1, 'Q2'),
    (1, 'Q3'),
    (1, 'Q5'),
    (1, 'Q7'),
    (1, 'Q8'),
    (1, 'R8'),
    (1, 'Rs3'),
    (1, 'Rs4'),
    (1, 'Rs5'),
    (1, 'Rs7'),
    (1, 'S3'),
    (1, 'S4'),
    (1, 'S4 Avant'),
    (1, 'S4 Cabriolet'),
    (1, 'S5'),
    (1, 'S7'),
    (1, 'S8'),
    (1, 'SQ5'),
    (1, 'SQ7'),
    (1, 'Tt'),
    (1, 'Tts'),
    (1, 'V8'),
    (2, 'M3'), 
    (2, 'M4'), 
    (2, 'M5'), 
    (2, 'M535'), 
    (2, 'M6'), 
    (2, 'M635'), 
    (2, 'Serie 1'), 
    (2, 'Serie 2'), 
    (2, 'Serie 3'), 
    (2, 'Serie 4'), 
    (2, 'Serie 5'),
    (2, 'Serie 6'), 
    (2, 'Serie 7'), 
    (2, 'Serie 8'),
    (3, 'C1'), 
    (3, 'C15'), 
    (3, 'C2'), 
    (3, 'C25'), 
    (3, 'C25D'), 
    (3, 'C25E'), 
    (3, 'C25TD'), 
    (3, 'C3'), 
    (3, 'C3 Aircross'), 
    (3, 'C3 Picasso'), 
    (3, 'C4'),
    (3, 'C4 Picasso'), 
    (3, 'C5'), 
    (3, 'C6'), 
    (3, 'C8'), 
    (3, 'Ds3'), 
    (3, 'Ds4'), 
    (3, 'Ds5');

INSERT INTO annonces(type_categorie_id, categorie_id, titre, contenu) VALUES
    (3, 8, 'Aud Incroyable', 'voiture à vendre pour voilier'),
    (3, 9, 'Audirait le sud', 'voiture à vendre pour charette'),
    (1, NULL, 'Developpeur Go', 'Developpeur Go qui correspondrait bien à Fabien Ogli'),
    (2, NULL, 'Logement à céder', 'Grande surface de 5m² pour seulement 1500€');
