# Requirements
* docker
* docker-compose

# Installation
```bash
docker-compose up
```

# Routes
| METHOD    | ROUTE            | BODY                                                                                                           |
|--------   |---------------   |------                                                                                                          |
| GET       | /annonces/:id    |                                                                                                                |
| DELETE    | /annonces/:id    |                                                                                                                |
| PUT       | /annonces/:id    |`{"titre": "titre", "contenu": "contenu", "type_categorie": "type_categorie", "categorie": "categorie"}`        |
| POST      | /annonces/:id    |`{"titre": "titre", "contenu": "contenu", "type_categorie": "type_categorie", "categorie": {"marque": "marque", "modele": "modele"}}`        |
| POST      | /search          |`{"keywords": "recherche à faire"}`                                                                             |

Pour les annonces concernant l'immobilier ainsi que les emplois, il n'y a pas besoin de préciser la catégorie.
Si en revanche l'on veut créer ou modifier une annonce correspondante à une automobile, il faut spécifier le modèle et la marque comme l'exemple du post au dessus.  
Tout les champs sont des strings, l'api recherche ensuite en BDD s'il existe bien.  

# Choix concernant l'architecture
Plutôt que la librairie Mux j'ai préféré utiliser Gin, qui est plus utilisé et plus maintenu  

# Difficultée rencontrées
J'ai perdu énormément de temps à me creuser la tête pour faire une bonne modélisation en base de donnée. Finalement, par manque de temps, j'ai choisi la solution actuelle pour avoir un produit fonctionnel.

# Lancement des tests
Toujours par manque de temps, je n'ai réalisé des tests que pour la recherche 
```bash
go test ./...
```

# Et la suite ?
Avec plus de temps, j'aurai implémenté les tests concernant les opérations CRUD de l'api pour pouvoir déplacer les fonctions et restructurer mieux le code