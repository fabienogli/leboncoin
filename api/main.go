package main

import (
	"log"

	"gitlab.com/fabienogli/leboncoin/api/models"
)

func main() {
	err := models.ConnectDatabase()
	if err != nil {
		log.Fatalf("Error while connecting to db %v", err)
	}
	r := GetRouter()
	r.Run()
}
