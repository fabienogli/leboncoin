package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/fabienogli/leboncoin/api/models"
)

func getAnnonceFromParamId(c *gin.Context) (models.Annonce, error) {
	var annonce models.Annonce
	orm := models.DB
	id := c.Param("id")
	u_id, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		log.Printf("Error Parsing id %v", err)
		return annonce, err
	}
	return models.GetAnnonce(orm, uint(u_id))
}

func getAnnonce(c *gin.Context) {
	annonce, err := getAnnonceFromParamId(c)
	if err != nil {
		log.Printf("Error while getting annonce %v", err)
		c.PureJSON(http.StatusInternalServerError, annonce)
		return
	}
	c.PureJSON(http.StatusOK, annonce)
}

func getAnnonces(c *gin.Context) {
	orm := models.DB
	annonces, err := models.GetAnnonces(orm)
	if err != nil {
		log.Printf("Error happened %v", err)
		c.PureJSON(http.StatusInternalServerError, "Oops")
		return
	}
	c.PureJSON(http.StatusOK, annonces)
}

func modifyAnnonce(c *gin.Context) {
	annonce, err := getAnnonceFromParamId(c)
	if err != nil {
		log.Printf("Error while modifying annonce %v", err)
		c.PureJSON(http.StatusInternalServerError, "Error")
		return
	}
	orm := models.DB
	var input models.AnnonceInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	annonce, err = models.ModifyAnnonce(orm, annonce.ID, input)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.PureJSON(http.StatusOK, annonce)
}

func createAnnonce(c *gin.Context) {
	orm := models.DB
	var input models.AnnonceInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Printf("received %v\n", input)

	annonce, err := models.CreateAnnonce(orm, input)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	c.PureJSON(http.StatusCreated, annonce)
}

type searchInput struct {
	Keywords string
}

func searchRelated(c *gin.Context) {
	db := models.DB
	var input searchInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	// a lot of optimisations to do but since for the exercice it's a small dataset, i load everything
	var autos []models.Automobile
	db.Order("id").Preload("Marque").Find(&autos)
	auto, _ := models.FindCategorie(input.Keywords, autos)
	// there is surely a better way to load the related annonces, but too little time
	var relatedAnnonces []models.Annonce
	db.Where("categorie_id = ?", auto.ID).Find(&relatedAnnonces)
	for i := 0; i < len(relatedAnnonces); i++ {
		relatedAnnonces[i].RetrieveCategorie(db)
	}
	c.JSON(http.StatusOK, relatedAnnonces)
}

func deleteAnnonce(c *gin.Context) {
	db := models.DB
	id := c.Param("id")
	u_id, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		log.Printf("error %v", u_id)
	}
	db.Delete(&models.Annonce{}, uint(u_id))
}

func GetRouter() *gin.Engine {

	router := gin.Default()

	router.GET("/annonces", getAnnonces)
	router.GET("/annonces/:id", getAnnonce)
	router.PUT("/annonces/:id", modifyAnnonce)
	router.DELETE("/annonces/:id", deleteAnnonce)
	router.POST("/annonces", createAnnonce)
	router.POST("/search", searchRelated)

	return router
}
