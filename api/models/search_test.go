package models

import (
	"testing"
)

var mockDbAudi = []string{"Cabriolet", "Q2", "Q3", "Q5", "Q7", "Q8", "R8", "Rs3", "Rs4", "Rs5", "Rs7", "S3", "S4", "S4 Avant", "S4 Cabriolet", "S5", "S7", "S8", "SQ5", "SQ7", "Tt", "Tts", "V8"}
var mockDbMBW = []string{"M3", "M4", "M5", "M535", "M6", "M635", "Serie 1", "Serie 2", "Serie 3", "Serie 4", "Serie 5", "Serie 6", "Serie 7", "Serie 8"}
var mockDbCitroen = []string{"C1", "C15", "C2", "C25", "C25D", "C25E", "C25TD", "C3", "C3 Aircross", "C3 Picasso", "C4", "C4 Picasso", "C5", "C6", "C8", "Ds3", "Ds4", "Ds5"}

var mockDB = [][]string{mockDbAudi, mockDbMBW, mockDbCitroen}

var searchBMW = "gran turismo serie 5"
var searchAudi = "rs4 avant"

var audi = Marque{
	Nom: "Audi",
}
var BMW = Marque{
	Nom: "Audi",
}
var Citroen = Marque{
	Nom: "Citroen",
}

func generateDbFromMarque(start int, marque Marque, models []string) []Automobile {
	var autos []Automobile
	for i, model := range models {
		autos = append(autos, Automobile{
			ID:     uint(start + i),
			Marque: marque,
			Modele: model,
		})
	}
	return autos
}

func generateDb() []Automobile {
	var autos []Automobile
	autos = append(autos, generateDbFromMarque(1, audi, mockDbAudi)...)
	autos = append(autos, generateDbFromMarque(len(autos), BMW, mockDbMBW)...)
	autos = append(autos, generateDbFromMarque(len(autos), Citroen, mockDbCitroen)...)
	return autos
}

func TestFindModel(t *testing.T) {
	autos := generateDb()
	found, error := FindCategorie("rs4 avant", autos)
	if error != nil {
		t.Errorf("Error searching %s avant %v", searchAudi, error)
	}
	audiExpected := "Rs4"
	if found.Modele != audiExpected {
		t.Errorf("Expected %v, got %v", audiExpected, found)
	}
	bmwExpected := "Serie 5"

	found, error = FindCategorie(searchBMW, autos)
	if error != nil {
		t.Errorf("gran turismo %s %v", searchBMW, error)
	}
	if found.Modele != bmwExpected {
		t.Errorf("Expected %v, got %v", bmwExpected, found)
	}
}
