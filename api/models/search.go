package models

import (
	"fmt"
	"log"
	"regexp"
	"strings"
)

//TODO need to do some cleaning here but no time

func FindCategorie(search string, modeles []Automobile) (Automobile, error) {
	var goodMatch Automobile
	var maxHit int
	matchCountHit := make(map[Automobile]int)
	searches := strings.Fields(search)
	for _, v := range searches {
		matches, err := FindSubCategorie(v, modeles)
		if err != nil {
			log.Printf("Error %v", err)
			return goodMatch, err
		}
		if len(matches) > 0 {
			for _, match := range matches {
				matchCountHit[match] += 1
			}
		}
	}
	for match, hitCount := range matchCountHit {
		if hitCount > maxHit {
			maxHit = hitCount
			goodMatch = match
		}
	}
	return goodMatch, nil
}

func FindSubCategorie(search string, modeles []Automobile) ([]Automobile, error) {
	var matches []Automobile
	re := fmt.Sprintf(`(?i)%s`, search)
	for _, v := range modeles {
		matched, err := regexp.MatchString(re, v.Modele)
		if err != nil {
			return nil, err
		}
		if matched {
			matches = append(matches, v)
		}
	}
	return matches, nil
}
