package models

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"

	"gorm.io/gorm"
)

type TypeCategorie struct {
	ID  uint   `json:"id" gorm:"primary_key"`
	Nom string `json:"nom"`
}

type Marque struct {
	ID  uint   `json:"id" gorm:"primary_key"`
	Nom string `json:"nom"`
}

type Automobile struct {
	ID       uint   `json:"id" gorm:"primary_key"`
	MarqueID uint   `json:"-"`
	Marque   Marque `gorm:"foreign_key:MarqueID"`
	Modele   string `json:"modele"`
}

type Annonce struct {
	ID              uint          `json:"id" gorm:"primary_key"`
	Titre           string        `json:"titre"`
	Contenu         string        `json:"contenu"`
	TypeCategorieID uint          `json:"-"`
	TypeCategorie   TypeCategorie `json:"type_categorie" gorm:"foreign_key"`
	CategorieID     uint          `json:"-"`
	Categorie       interface{}   `json:"categorie,omitempty" gorm:"-"`
}

type AnnonceInput struct {
	Titre         string                 `json:"titre" binding:"required"`
	Contenu       string                 `json:"contenu" binding:"required"`
	TypeCategorie string                 `json:"type_categorie" binding:"required"`
	Categorie     map[string]interface{} `json:"categorie"`
}

type AutomobileInput struct {
	Marque string
	Modele string
}

func (a *Annonce) RetrieveCategorie(db *gorm.DB) error {
	if a.CategorieID == 0 {
		log.Printf("No categorie inside annonce\n%v", a)
		return nil
	}
	if a.TypeCategorieID == 0 {
		errorMsg := fmt.Sprintf("No type categorie inside annonce\n%v", a)
		a.Categorie = nil
		return errors.New(errorMsg)
	}
	if a.TypeCategorie.Nom == "" {
		db.Find(&a.TypeCategorie, a.TypeCategorieID)
	}
	switch a.TypeCategorieID {
	case 3:
		var auto Automobile
		db.Preload("Marque").Find(&auto, a.CategorieID)
		a.Categorie = auto
	}
	return nil
}

func GetAnnonce(db *gorm.DB, id uint) (Annonce, error) {
	var annonce Annonce
	log.Printf("getting annonce %d", id)
	db.Preload("TypeCategorie").First(&annonce, id)
	err := annonce.RetrieveCategorie(db)
	return annonce, err
}

func GetAnnonces(db *gorm.DB) ([]Annonce, error) {
	var annonces []Annonce
	db.Preload("TypeCategorie").Find(&annonces)
	for i := 0; i < len(annonces); i++ {
		err := annonces[i].RetrieveCategorie(db)
		if err != nil {
			return nil, err
		}
	}
	return annonces, nil
}

func (annonce *Annonce) getAutomobileData(db *gorm.DB, input AnnonceInput) error {
	jsonbody, err := json.Marshal(input.Categorie)
	if err != nil {
		errorMsg := fmt.Sprintf("Couldn't marshall following %v\n", input.Categorie)
		return errors.New(errorMsg)
	}
	var automobileInput AutomobileInput
	err = json.Unmarshal(jsonbody, &automobileInput)
	if err != nil {
		errorMsg := fmt.Sprintf("Couldn't unmarshal %v\nWas originally this\n%v", jsonbody, input.Categorie)
		return errors.New(errorMsg)
	}
	var automobile Automobile
	db.Where("modele = ?", automobileInput.Modele).First(&automobile)
	if automobile.ID == 0 {
		errorMsg := fmt.Sprintf("Automobile not found for %v", automobileInput)
		return errors.New(errorMsg)
	}
	annonce.CategorieID = automobile.ID
	return nil
}

func (annonce *Annonce) GetRelatedData(db *gorm.DB, input AnnonceInput) error {
	var typeCategorie TypeCategorie
	db.Where("nom = ?", input.TypeCategorie).First(&typeCategorie)
	if typeCategorie.ID == 0 {
		var categories []TypeCategorie
		db.Find(&categories)
		errorMsg := fmt.Sprintf("La categorie '%s' n'existe pas", input.TypeCategorie)
		return errors.New(errorMsg)
	}
	annonce.TypeCategorieID = typeCategorie.ID

	if typeCategorie.ID == 3 {
		annonce.getAutomobileData(db, input)
	}
	return nil
}

func ModifyAnnonce(db *gorm.DB, id uint, input AnnonceInput) (Annonce, error) {
	newAnnonce := Annonce{
		ID:      id,
		Titre:   input.Titre,
		Contenu: input.Contenu,
	}
	err := newAnnonce.GetRelatedData(db, input)
	if err != nil {
		return newAnnonce, err
	}
	db.Save(&newAnnonce)
	return GetAnnonce(db, newAnnonce.ID)
}

func CreateAnnonce(db *gorm.DB, input AnnonceInput) (Annonce, error) {
	newAnnonce := Annonce{
		Titre:   input.Titre,
		Contenu: input.Contenu,
	}
	err := newAnnonce.GetRelatedData(db, input)
	if err != nil {
		return newAnnonce, err
	}
	db.Create(&newAnnonce)
	return GetAnnonce(db, newAnnonce.ID)
}
